#!/usr/bin/python





class Node:
    
    numberOfNodes = 0
    
    def __init__(self, name):
        self.name = name
        Node.numberOfNodes += 1
    
    def displayCount(self):
        print "Total number of nodes:  %d" % Node.numberOfNodes
    
    def displayNode(self):
        print "Name of Node: " , self.name


node1 = Node(